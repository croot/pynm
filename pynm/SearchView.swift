//
//  Search.swift
//  pynm
//
//  Created by 최근호 on 2018. 7. 7..
//  Copyright © 2018년 최근호. All rights reserved.
//

import UIKit

class SearchView: UIView {
    
    var searchViewProto: CGRect!
    
    @IBOutlet var searchBtn: UIButton!
    @IBOutlet var searchFilter: UIView!
    @IBOutlet var filterRating: UISlider!
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    func matches(for regex: String, in text: String) -> Bool {
        
        do {
            let regex = try NSRegularExpression(pattern: regex)
            let results = regex.matches(in: text, range: NSRange(text.startIndex..., in: text))
            return results.count > 0
        } catch let error {
            print("invalid regex: \(error.localizedDescription)")
            return false
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.searchViewProto = frame
        self.searchFilter.isHidden = true
        self.hideSearchView()
        
        //let image:UIImage = UIImage(named: "")
        //filterRating.setThumbImage(image, for: .normal)
    }
    
    func showSearchView() {
        self.isHidden = false
        UIView.animate(withDuration: 0.5, animations: {
            //self.searchView.alpha = 1
            if self.searchViewProto != nil {
                self.frame = self.searchViewProto
            }
        })
    }
    
    func hideSearchView() {
        UIView.animate(withDuration: 0.5, animations: {
            //self.searchView.alpha = 0
            self.frame = self.searchBtn.frame
        }, completion:{ _ in
            self.isHidden = true
        })
    }
    
    func showSearchFilterView() {
        self.searchFilter.isHidden = false
        UIView.animate(withDuration: 0.3, animations: {
            self.searchFilter.frame.size.height = 140
        })
    }
    
    func hideSearchFilterView() {
        UIView.animate(withDuration: 0.3, animations: {
            self.searchFilter.frame.size.height = 0
        }, completion: { _ in
            self.searchFilter.isHidden = true
        })
    }
    
    
    
    @IBAction func searchBoxToggle(_ sender: Any) {
        
        self.isHidden = !self.isHidden
        
        if self.isHidden == false {
            self.showSearchView()
        } else {
            self.hideSearchView()
            self.hideSearchFilterView()
        }
    }
    
    @IBAction func searchFilterToggle(_ sender: Any) {
        self.searchFilter.isHidden = !self.searchFilter.isHidden
        
        if self.searchFilter.isHidden == false {
            self.showSearchFilterView()
        } else {
            self.hideSearchFilterView()
        }
    }
}
