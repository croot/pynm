//
//  PopoverView.swift
//  pynm
//
//  Created by 최근호 on 2018. 7. 4..
//  Copyright © 2018년 최근호. All rights reserved.
//

import UIKit

class PopoverView: UIView {
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    @IBOutlet var name: UILabel!
    @IBOutlet var address: UILabel!
    @IBOutlet var tel: UILabel!
    @IBOutlet var star: UILabel!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        print(self.superview!.frame.height / 2)
        print(self.frame.height)
        self.frame.origin.y = (self.superview!.frame.height / 2) - self.frame.height - 140
        layer.masksToBounds = false
        layer.shadowOffset = CGSize(width: -1, height: 3)
        layer.shadowRadius = 1
        layer.shadowOpacity = 0.4
    }
    
    func showPopover(data: StoreData) {
        self.appDelegate.isAutoCenter = false
        self.isHidden = false
        self.name.text = data.name
        self.address.text = "\(data.address1) \(data.address2) \(data.address3)"
        self.tel.text = data.tel
        self.star.text = ""
        if let star = data.star {
            for _ in (0...Int(floorf(star))) {
                //self.star.text? += #imageLiteral(resourceName: "star-filled")
            }
            if roundf(star) != star {
                //self.star.text? += #imageLiteral(resourceName: "star-half")
            }
        }
        
    }
    
    func hidePopover() {
        self.isHidden = true
    }
}
